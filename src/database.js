const mysql = require('mysql');
const { promisify } = require('util');
const { database } = require('./keys');
const pool = mysql.createPool(database);

pool.getConnection((err, connection) => {
    if (err) {
        console.log(err.message);
    } else if (connection) {
        connection.release();

        /* Crear tabla usuarios */
        pool.query('CREATE TABLE IF NOT EXISTS users(id int(11) not null auto_increment, username varchar(16) not null, password varchar(60) not null,fullname varchar(100) not null,UNIQUE(username), PRIMARY KEY (id));');

        /* Crear tabla links */
        pool.query('CREATE TABLE IF NOT EXISTS links(id int(11) not null auto_increment, title varchar(150) not null, url varchar(255) not null, description TEXT, user_id int(11), created_at timestamp not null default current_timestamp, constraint fk_user foreign key (user_id) references users(id), primary key (id));');

        console.log('DB is Connected');

        return;
    }
});

// promisify pool querys
pool.query = promisify(pool.query);

module.exports = pool;